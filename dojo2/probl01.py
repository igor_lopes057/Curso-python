from unittest import TestCase
from typing import Text


def jogo_jokenpo(mao_jogador1: Text, mao_jogador2: Text) -> Text:
    """
    Jogo do jokenpo

    Regras de entrada: Cada jogador escolhe um elemento valido(pedra, tesoura, papel). O resultado será
    avaliado com base em regras pré-definidas:
        - Pedra ganha de tesoura;
        - Tesoura ganha de Papel;
        - Papel ganha de pedra;
        - Entradas iguais empata.
    Caso o elemento não esteja dentro da lista, retorna invalido.

    >>> jogo_jokenpo('tesoura', 'tesoura')
    'Empate'

    >>> jogo_jokenpo('vassoura', 'papel')
    'Argumento inválido'

    >>> jogo_jokenpo('Pedra', 'tesoura')
    'Pedra Ganhou'

    """
    dicionario = {'pedra': 'papel', 'papel': 'tesoura', 'tesoura': 'pedra'}

    mao_jogador1, mao_jogador2 = mao_jogador1.lower(),  mao_jogador2.lower()

    if mao_jogador1 not in ["pedra", "papel", "tesoura",] or mao_jogador2 not in ["pedra", "papel", "tesoura"]:

        return "Argumento inválido"

    if mao_jogador1 == mao_jogador2:
        return "Empate"

    if dicionario[mao_jogador1] == mao_jogador2:
        return mao_jogador2.capitalize() + " Ganhou"
    else:
        return mao_jogador1.capitalize() + " Ganhou"


class TestJokenpo_empate(TestCase):
    def test_papel_papel_deve_retornar_empate(self):
        self.assertEqual(jogo_jokenpo("papel", "papel"), "Empate")

class TestJokenpo_papelwins(TestCase):
    def test_pedra_papel_deve_retornar_papel(self):
        self.assertEqual(jogo_jokenpo("pedra", "papel"), "Papel Ganhou")

    def test_papel_pedra_deve_retornar_papel(self):
        self.assertEqual(jogo_jokenpo("papel", "pedra"), "Papel Ganhou")

class TestJokenpo_tesourawins(TestCase):
    def test_papel_tesoura_deve_retornar_tesoura(self):
        self.assertEqual(jogo_jokenpo("papel", "tesoura"), "Tesoura Ganhou")

    def test_tesoura_papel_deve_retornar_tesoura(self):
        self.assertEqual(jogo_jokenpo("tesoura", "papel"), "Tesoura Ganhou")

class TestJokenpo_pedrawins(TestCase):
    def test_tesoura_pedra_deve_retornar_pedra(self):
        self.assertEqual(jogo_jokenpo("tesoura", "pedra"), "Pedra Ganhou")

    def test_pedra_tesoura_deve_retornar_pedra(self):
        self.assertEqual(jogo_jokenpo("pedra", "tesoura"), "Pedra Ganhou")

    def test_pedra_tesoura_capital_case_deve_retornar_pedra(self):
        self.assertEqual(jogo_jokenpo("pedra", "Tesoura"), "Pedra Ganhou")

class TestJokenpo_argumentoInvalido(TestCase):
    def test_ArgumentInvalido(self):
        self.assertEqual(jogo_jokenpo(1, "tesoura"), "Argumento inválido")

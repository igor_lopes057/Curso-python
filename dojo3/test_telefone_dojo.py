from unittest import TestCase
from telefone_dojo import telefone_antigo

class Test_dojo_3(TestCase):
    def teste_recebe_1_retorna_1(self):
        self.assertEqual(telefone_antigo('1'), '1')

    def teste_recebe_A_retorna_2(self):
        self.assertEqual(telefone_antigo('A'), '2')

    def teste_recebe_cenoura_retorna_2366872(self):
        self.assertEqual(telefone_antigo('CENOURA'), '2366872')

    def teste_recebe_marmita_minusculo_retorna_argumento_invalido(self):
        self.assertEqual(telefone_antigo('marmita'), 'Argumento invalido')

    def teste_recebe_exclamaçao_retorna_argumento_invalido(self):
        self.assertEqual(telefone_antigo('!'), 'Argumento invalido')

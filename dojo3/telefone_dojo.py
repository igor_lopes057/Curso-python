from string import ascii_uppercase

def tabela_telefone(frase):
    if frase in 'ABC':
        return '2'
    if frase in 'DEF':
        return '3'
    if frase in 'GHI':
        return '4'
    if frase in 'JKL':
        return '5'
    if frase in 'MNO':
        return '6'
    if frase in 'PQRS':
        return '7'
    if frase in 'TUV':
        return '8'
    if frase in 'WXYZ':
        return '9'
    if frase in '01-':
        return frase

def telefone_antigo(frase: str) -> str:
    """ Dado um telefone antigo, onde não há um teclado qwert e eu precise passar
    uma mensagem. O meu texto será convertido com base na tabela:
    Letras  ->  Número
    ABC    ->  2
    DEF    ->  3
    GHI    ->  4
    JKL    ->  5
    MNO    ->  6
    PQRS   ->  7
    TUV    ->  8
    WXYZ   ->  9
    -      -> -
    0      -> 0
    1      -> 1
    
    A função não aceita outros caracteres além dos da tabela ou letras minusculas.
    >>> telefone_antigo('!')
    'Argumento invalido'
    >>> telefone_antigo('cenoura')
    'Argumento invalido'
    >>> telefone('CENOURA')
    '2366872' """
    for element in frase:
        if element not in ascii_uppercase + '01-':
            return 'Argumento invalido'

    return "".join(map(tabela_telefone, frase))

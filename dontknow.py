"""
behave <nome-arquivo>  #BDD
python3 -m unittest <nome-arquivo> -v #TDD
radon cc <nome-arquivo> #Complexidade

export FLASK_APP=app #FLASK
# Para ver as rotas
flask routes
# Para rodar
flask run

# Para rodar os testes
python -m unittest discover tests/ -v

#CRIANDO UM AMBIENTE VIRTUAL(VIRTUALENV):
pip install virtualenv
virtualenv virtual (criar na pasta do projeto)
source virtual/bin/activate (Encontrar o activat na pasta bin para isolar o projeto)
"""

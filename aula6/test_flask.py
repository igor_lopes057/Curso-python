from unittest import TestCase
from flask import url_for
from app import app

class TestHome(TestCase):
    def setUp(self):
        self.app = app
        self.app.testing = True
        self.app_context = self.app.test_request_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def test_home_deve_retornar_sc_200(self):
        esperado = 200 #STATUS CODE: OK
        response = self.client.get(url_for('ola_jovens'))
        self.assertEqual(response.status_code, esperado)

    def test_hello_user_deve_retornar_sc_200(self):
        user = 'Batman'
        esperado = 200
        response = self.client.get(url_for('iam', user=user))
        self.assertEqual(response.status_code, esperado)

from flask import Flask

app = Flask(__name__)

@app.route('/', methods=['GET'])
def ola_jovens():
    return 'Olá jovens!'

@app.route('/iam/<user>', methods=['GET'])
def iam(user):
    return f'Eu sou o {user}'

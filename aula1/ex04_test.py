from unittest import TestCase, main
from ex04 import calculo1, calculo2, calculo3

class TestEx04_calculo01(TestCase):
    def teste01(self):
        self.assertEqual(calculo1(1, 2), 2)
    
    def teste02(self):
        self.assertEqual(calculo1(2, 3), 6)

    def teste03(self):
        self.assertEqual(calculo1(5,2), 10)

class TestEx04_calculo02(TestCase):
    def teste04(self):
        self.assertEqual(calculo2(1, 2), 5)
    
    def teste05(self):
        self.assertEqual(calculo2(2, 3), 9)

    def teste06(self):
        self.assertEqual(calculo2(5, 2), 17)

class TestEx04_calculo03(TestCase):
    def teste07(self):
        self.assertEqual(calculo3(1), 1)
    
    def teste08(self):
        self.assertEqual(calculo3(2), 8)

    def teste09(self):
        self.assertEqual(calculo3(5), 125)
from unittest import TestCase, main
from ex05 import media

class TestEx05(TestCase):
    def teste01(self):
        self.assertEqual(media(7,7), 'Aprovado')
    
    def teste02(self):
        self.assertEqual(media(6,5), 'Reprovado')

    def teste03(self):
        self.assertEqual(media(3,3), 'Reprovado')

    def teste04(self):
        self.assertEqual(media(8,8), 'Aprovado')
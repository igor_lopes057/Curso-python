palavra = str(input("Digite uma palavra ou frase: "))

if 'a' in palavra or 'e' in palavra or 'i' in palavra or 'o' in palavra or 'u' in palavra:
    print("Apresenta alguma vogal.")
else:
    print("Não apresenta nenhuma vogal.")

if palavra.find(' ') == 1:
    print("É uma frase.")
else:
    print("Não é uma frase.")
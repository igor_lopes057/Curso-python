from unittest import TestCase, main
from ex18 import quantil

class TestEx18(TestCase):
    def teste01(self):
        self.assertEqual(quantil([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 0.90), 10)
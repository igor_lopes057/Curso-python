metros = int(input("Digite quantos metros será usado: "))
litros = metros/3

capacidade = 18
preco = 80.00

lata = int(litros / capacidade)
precoVenda = lata * preco

print("Você usará {} latas.".format(lata))
print("O custo total será de R${}".format(precoVenda))

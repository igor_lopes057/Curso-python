from unittest import TestCase, main
from ex21 import dicion

class TestEx21(TestCase):
    def teste01(self):
        self.assertEqual(dicion, {'lista': [1, 2, 3, 4, 5], 'quadrado': [1, 4, 9, 16, 25], 'cubo': [1, 8, 27, 64, 125]})
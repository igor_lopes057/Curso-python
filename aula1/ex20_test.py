from unittest import TestCase, main
from ex20 import lista1

class TestEx20(TestCase):
    def teste01(self):
        self.assertEqual(lista1, {'lista': [1, 2, 3, 4], 'somatoria': 10, 'tamanho': 4, 'maior': 4, 'menor': 1})
from unittest import TestCase, main
from ex15 import listaEntrada

class TestEx15(TestCase):
    def teste01(self):
        self.assertEqual(listaEntrada, [(1, 1), (1, 2), (1, 3), (1, 4), (2, 2), (2, 3), (2, 4), (3, 3), (3, 4), (4, 4)])
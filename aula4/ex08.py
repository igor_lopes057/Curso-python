def sabores_de_pizza():
    sabor = [('Mussarela', 14.99),
    ('Frango com catupiry', 24.99),
    ('Calabresa', 13.99),
    ('Portuguesa', 29.99),
    ('Moda da casa', 34.99),
    ('Baiana', 19.99)]
    return sabor

def get_sabores_de_pizza(valor):
    return list(filter(lambda x: x[1] <= valor, sabores_de_pizza()))

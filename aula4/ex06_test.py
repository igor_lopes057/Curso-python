from unittest import TestCase, mock
from ex06 import filtroUsuario

class Test_getusers(TestCase):
    def test01(self):
        self.assertEqual(filtroUsuario(100), [(507, 'Jorgin', 'jorgin_gatin@hotmail.com'),
        (102, 'Cleyson', 'cleysao.hot@gmail.com'),
        (107, 'Luan', 'luangameplays@yahoo.com'),
        (157, 'Mano Brown', 'estilocachorro@hotmail.com')])

    def test02(self):
        self.assertEqual(filtroUsuario(150), [(507, 'Jorgin', 'jorgin_gatin@hotmail.com'),
        (157, 'Mano Brown', 'estilocachorro@hotmail.com')])

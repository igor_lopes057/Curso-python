from unittest import TestCase
from ex03 import freqn

class TestFrequenciaLetras(TestCase):
    def testeFrequencia_python(self):
        self.assertEqual(freqn('python'), {'p': 1, 'y': 1, 't': 1, 'h': 1, 'o': 1, 'n': 1})

    def testeFrequencia__(self):
        self.assertEqual(freqn(' '), {' ': 1})

    def testeFrequencia_batataBatata(self):
        self.assertEqual(freqn('batataBatata'), {'B': 1, 'b': 1, 'a': 6, 't': 4})

from unittest import TestCase
from ex02 import soma, sub, exp

class TestExp_soma(TestCase):
    def teste_Soma01(self):
        self.assertEqual(soma(1, 2), 3)

    def teste_Soma02(self):
        self.assertEqual(soma([], [2]), [2])

    def teste_Soma03(self):
        self.assertEqual(soma('olá', 5), 'Não é possivel calcular.')

class TestExp_sub(TestCase):
    def teste_Sub01(self):
        self.assertEqual(sub(2, 1), 1)

    def teste_Sub02(self):
        self.assertEqual(sub(1, -1), 2)

class TestExp_Exp(TestCase):
    def teste_exp01(self):
        self.assertEqual(exp(1, 2, 3), 0)

    def teste_exp02(self):
        self.assertEqual(exp(lambda x: x+1, [2], 'eduardo'), 'Não foi possivel calcular.')

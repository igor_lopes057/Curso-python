from unittest import TestCase, mock
from ex08 import get_sabores_de_pizza

class TestSaboresPizza(TestCase):
    def testeValor_menor_18(self):
        self.assertEqual(get_sabores_de_pizza(18), [('Mussarela', 14.99), ('Calabresa', 13.99)])

    def testeValor_menor_35(self):
        self.assertEqual(get_sabores_de_pizza(35), [('Mussarela', 14.99),
        ('Frango com catupiry', 24.99),
        ('Calabresa', 13.99),
        ('Portuguesa', 29.99),
        ('Moda da casa', 34.99),
        ('Baiana', 19.99)])

class TestSaboresPizza_with_mock(TestCase):
    def testeValor_menor_18_mock(self):
        with mock.patch('ex08_test.get_sabores_de_pizza') as mocked_pizza:
           get_sabores_de_pizza([('Mussarela', 14.99), ('Calabresa', 13.99)])

        mocked_pizza.assert_called_with(get_sabores_de_pizza(18))

    def testeValor_menor_35_mock(self):
        with mock.patch('ex08_test.get_sabores_de_pizza') as mocked_pizza:
            get_sabores_de_pizza([('Mussarela', 14.99), ('Frango com catupiry', 24.99), ('Calabresa', 13.99), ('Portuguesa', 29.99), ('Moda da casa', 34.99), ('Baiana', 19.99)])

        mocked_pizza.assert_called_with(get_sabores_de_pizza(35))

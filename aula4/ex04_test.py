from unittest import TestCase
from ex04 import transform

class TestStringintoList(TestCase):
    def teste_python(self):
        self.assertEqual(transform('Python'), ['Python'])

    def teste_PythonLove(self):
        self.assertEqual(transform('Python love'), ['Python', 'love'])

    def teste_PythonFuck(self):
        self.assertEqual(transform('Python é foda'), ['Python', 'é', 'foda'])

    def teste_Visual_Studio(self):
        self.assertEqual(transform('Visual Studio é ruim'), ['Visual', 'Studio', 'é', 'ruim'])

from unittest import TestCase
from ex05 import freqn, transform, contarListas

class TestFrequenciaLetras(TestCase):
    def testeFrequencia_python(self):
        self.assertEqual(freqn('python'), {'p': 1, 'y': 1, 't': 1, 'h': 1, 'o': 1, 'n': 1})

    def testeFrequencia__(self):
        self.assertEqual(freqn(' '), {' ': 1})

    def testeFrequencia_batataBatata(self):
        self.assertEqual(freqn('batataBatata'), {'B': 1, 'b': 1, 'a': 6, 't': 4})

class TestStringintoList(TestCase):
    def teste_python(self):
        self.assertEqual(transform('Python'), ['Python'])

    def teste_PythonLove(self):
        self.assertEqual(transform('Python love'), ['Python', 'love'])

    def teste_PythonFuck(self):
        self.assertEqual(transform('Python é foda'), ['Python', 'é', 'foda'])

    def teste_Visual_Studio(self):
        self.assertEqual(transform('Visual Studio é ruim'), ['Visual', 'Studio', 'é', 'ruim'])

class TestContarListas(TestCase):
    def teste_ListaPython(self):
        self.assertEqual(contarListas(['Python']), {'P': 1, 'y': 1, 't': 1, 'h': 1, 'o': 1, 'n': 1})

    def teste_ListaPythonLove(self):
        self.assertEqual(contarListas(['Python love']), {' ': 1, 'P': 1, 'y': 1, 't': 1, 'h': 1, 'o': 2, 'n': 1, 'l': 1, 'v': 1, 'e': 1})

def get_users():
    users = [(507, 'Jorgin', 'jorgin_gatin@hotmail.com'),
    (102, 'Cleyson', 'cleysao.hot@gmail.com'),
    (107, 'Luan', 'luangameplays@yahoo.com'),
    (50, 'Bettina', 'battina_rica@ig.com'),
    (157, 'Mano Brown', 'estilocachorro@hotmail.com'),
    (13, 'Felipe', 'monstro.memo13@gmail.com')]
    return users

def filtroUsuario(id):
    return list(filter(lambda x: x[0] > id, get_users()))

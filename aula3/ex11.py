from unittest import TestCase, mock
from ex07 import exp

class Test_ex11(TestCase):
    def teste_expres_01(self):
        with mock.patch('ex07.soma') as mocked_soma:
            exp(1 ,2, 3)
        mocked_soma.assert_called_with(1, 2)

    def teste_expres_02(self):
        with mock.patch('ex07.sub') as mocked_sub:
           exp(0, 0, 3)
        mocked_sub.assert_called_with(0,3)

from unittest import TestCase, main
from ex07 import exp

class Test_ex07(TestCase):
    def teste_expres_01(self):
        self.assertEqual(exp(1, 2, 3), 0)

    def teste_expres_02(self):
        self.assertEqual(exp(1, 3, 2), 2)
class Calc:
  def soma(self, x, y):
    return x + y

  def sub(self, x, y):
    return x + y

  def mult(self, x, y):
    return x + y

  def div(self, x, y):
    return x + y

assert Calc().soma(2,2) == 4
assert Calc().soma(3,2) == 5
assert Calc().sub(-2,1) == -3
assert Calc().sub(2,-3) == 5
assert Calc().mult(2,2) == 4
assert Calc().mult(3,2) == 6
assert Calc().div(4,2) == 2
assert Calc().div(2,2) == 1
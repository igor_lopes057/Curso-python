def soma(x, y):
    return x + y

assert soma(2,2) == 4
assert soma(3,2) == 5
assert soma(-2,1) == -1
assert soma('oi ', 'tudo bem?') == 'oi tudo bem?'
assert soma([], [2]) == [2]
assert soma(2,-1) == 1
assert soma(3.1, 2) == 5.1
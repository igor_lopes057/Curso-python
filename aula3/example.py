class TestCasoAdd(TestCase):
   def test_add(self):
       cases = [(0, 0, 0), (1, 1, 2), (2, -2, 0)]
       for x, y, result in cases:
           with self.subTest(x=x, y=y, resultado=result):
               self.assertEqual(Calc().add(x, y), result)
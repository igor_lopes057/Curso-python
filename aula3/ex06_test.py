from unittest import TestCase, main
from ex06 import Calc

class Test_ex03_soma(TestCase):
    def teste_soma_01(self):
        self.assertEqual(Calc().soma(1,2), 3)

    def teste_soma_02(self):
        self.assertEqual(Calc().soma(-1,2), 1)
    
class Test_ex03_sub(TestCase):
    def teste_sub_01(self):
        self.assertEqual(Calc().sub(1,2), -1)

    def teste_sub_02(self):
        self.assertEqual(Calc().sub(-1,-1), 0)

class Test_ex03_multp(TestCase):
    def teste_multp_01(self):
        self.assertEqual(Calc().mult(3,2), 6)

    def teste_multp_02(self):
        self.assertEqual(Calc().mult(2,2), 4)

class Test_ex03_div(TestCase):
    def teste_div_01(self):
        self.assertEqual(Calc().div(2,2), 1)

    def teste_div_02(self):
        self.assertEqual(Calc().div(4,2), 2)

#Testes para cada operação individual da classe Calc.
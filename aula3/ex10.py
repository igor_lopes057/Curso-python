from unittest import TestCase, main
from ex07 import exp
from typing import NewType, Any

Dummy = NewType('Dummy', Any)

class Test_ex07(TestCase):
    def teste_expres_01(self):
        self.assertEqual(exp(Dummy(1), 2, 3), 0)
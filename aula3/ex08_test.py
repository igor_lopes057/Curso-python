from unittest import TestCase, main
from ex08 import new_exp, exp

class Test_ex07(TestCase):
    def teste_expres_01(self):
        self.assertEqual(new_exp(exp(1, 2, 3), 2, 3), -1)

    def teste_expres_02(self):
        self.assertEqual(new_exp(exp(1, 3, 2), 3, 2), 3)
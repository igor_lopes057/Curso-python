from unittest import TestCase, main
from ex10 import igualdade

class TestLista(TestCase):
    def teste01(self):
        self.assertEqual(igualdade('aaa', 3), True)

    def teste02(self):
        self.assertEqual(igualdade('oi', 2), True)

    def teste03(self):
        self.assertEqual(igualdade('marmita', 7), True)
from unittest import TestCase, main
from ex12 import multiplos_3

class Test_Ex12(TestCase):
    def teste01(self):
        self.assertEqual(multiplos_3(3), 'queijo')

    def teste02(self):
        self.assertEqual(multiplos_3(5), 5)

    def teste03(self):
        self.assertEqual(multiplos_3(6), 'queijo')

    def teste04(self):
        self.assertEqual(multiplos_3(15), 'queijo')

    def teste05(self):
        self.assertEqual(multiplos_3(2), 2)
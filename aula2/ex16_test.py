from unittest import TestCase, main
from ex16 import aplica_em_lote, soma_1

class Test_ex16(TestCase):
    def teste01(self):
        self.assertEqual(aplica_em_lote(soma_1, 3), [1, 2, 3])
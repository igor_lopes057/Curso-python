def multiplos_3and5(value):
    return 'romeu e julieta' if value%3 == 0 and value%5 == 0 else value

# assert multiplos_3and5(3) == 3
# assert multiplos_3and5(5) == 5
# assert multiplos_3and5(6) == 6
# assert multiplos_3and5(15) == 'romeu e julieta'
# assert multiplos_3and5(30) == 'romeu e julieta'

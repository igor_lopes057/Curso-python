from unittest import TestCase, main
from ex17 import aplica, soma_1, sub_1

class Test_ex17(TestCase):
    def teste_soma(self):
        self.assertEqual(aplica(soma_1, [1, 2, 3]), [2, 3, 4])

    def teste_sub(self):
        self.assertEqual(aplica(sub_1, [1, 2, 3]), [0, 1, 2])
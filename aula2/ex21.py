import functools

assert len(functools.reduce(lambda x, y: x+y, ['keep', 'remove', 'keep', 'remove', 'Keep', 'remove'])) == 30

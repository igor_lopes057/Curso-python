def multiplos_3(value):
    return 'queijo' if value % 3 == 0 else value

# assert multiplos_3(3) == 'queijo'
# assert multiplos_3(5) == 5
# assert multiplos_3(6) == 'queijo'
# assert multiplos_3(15) == 'queijo'
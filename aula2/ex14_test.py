from unittest import TestCase, main
from ex14 import multiplos_3and5

class Test_Ex14(TestCase):
    def teste01(self):
        self.assertEqual(multiplos_3and5(3), 3)

    def teste02(self):
        self.assertEqual(multiplos_3and5(5), 5)

    def teste03(self):
        self.assertEqual(multiplos_3and5(6), 6)

    def teste04(self):
        self.assertEqual(multiplos_3and5(15), 'romeu e julieta')

    def teste05(self):
        self.assertEqual(multiplos_3and5(2), 2)
def multiplos_5(value):
    return 'goiabada' if value%5 == 0 else value

# assert multiplos_5(3) == 3
# assert multiplos_5(5) == 'goiabada'
# assert multiplos_5(6) == 6
# assert multiplos_5(15) == 'goiabada'
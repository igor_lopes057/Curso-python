from unittest import TestCase, main
from ex18 import comp

class Test_ex18_multip_3(TestCase):
    def teste01(self):
        self.assertEqual(comp('multiplo_3', 3), True)

    def teste02(self):
        self.assertEqual(comp('multiplo_3', 5), False)

class Test_ex18_multip_5(TestCase):
    def teste03(self):
        self.assertEqual(comp('multiplo_5', 5), True)

    def teste04(self):
        self.assertEqual(comp('multiplo_5', 3), False)

class Test_ex18_multip_3and5(TestCase):
    def teste05(self):
        self.assertEqual(comp('multiplo_3and5', 15), True)

    def teste06(self):
        self.assertEqual(comp('multiplo_3and5', 19), False)
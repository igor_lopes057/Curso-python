from unittest import TestCase, main
from ex15 import reaplica, soma_2, sub_2

class Test_Ex15_Soma(TestCase):
    def teste01(self):
        self.assertEqual(reaplica(soma_2, 2), 6)

    def teste02(self):
        self.assertEqual(reaplica(soma_2, 4), 8)


class Test_Ex15_Sub(TestCase):
    def teste03(self):
        self.assertEqual(reaplica(sub_2, 2), -2)

    def teste04(self):
        self.assertEqual(reaplica(sub_2, 4), 0)
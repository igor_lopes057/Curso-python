from unittest import TestCase, main
from ex13 import multiplos_5

class Test_Ex13(TestCase):
    def teste01(self):
        self.assertEqual(multiplos_5(3), 3)

    def teste02(self):
        self.assertEqual(multiplos_5(5), 'goiabada')

    def teste03(self):
        self.assertEqual(multiplos_5(6), 6)

    def teste04(self):
        self.assertEqual(multiplos_5(15), 'goiabada')

    def teste05(self):
        self.assertEqual(multiplos_5(2), 2)
def comp(ID, value):
    union = {
        'multiplo_3': lambda value: value %3 == 0,
        'multiplo_5': lambda value: value %5 == 0,
        'multiplo_3and5': lambda value: value %3 == 0 and value %5 == 0
    }
    return union[ID](value)
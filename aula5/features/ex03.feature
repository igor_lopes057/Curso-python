# language:pt
Funcionalidade: Jogo de Jokenpo
  A fim de passar o tempo ou decidir o primeiro a tirar time no futebol,
  eu como um jogador, escolho entre um elemento(pedra, papel ou tesoura) contra o outro jogador.
  Caso empate a jogada do jokenpo, os dois jogadores recomeçam a partida.

#Cenario onde os dois empatam
Cenário: Capt. A joga "pedra" e Capt. B joga "pedra"
  Quando Capt. A joga "pedra" e Capt. B joga "pedra"
  Então acontece um "Empate" e eles jogam novamente

#Cenario onde Capt. A ganha
Cenário: Capt. A joga "tesoura" e Capt. B joga "papel"
  Quando Capt. A joga "tesoura" e Capt. B joga "papel"
  Então Capt. A ganhou com "tesoura"

#Cenario onde o Capt. B ganha
Cenário: Capt. A joga "PeDrA" e Capt. B joga "paPel"
  Quando Capt. A joga "PeDrA" e Capt. B joga "paPel"
  Então Capt. B ganhou com "papel"

 #Cenario onde o Capt. A joga 1
Cenário: Capt. A errou o jogo e jogou 1
  Quando Capt. A joga "1" e Capt. B joga "Tesoura"
    Então não é possivel jogar, retorna "Argumento inválido"

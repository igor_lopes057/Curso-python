from typing import Text
from behave import when, then

def jogo_jokenpo(mao_jogador1: Text, mao_jogador2: Text) -> Text:

    dicionario = {'pedra': 'papel', 'papel': 'tesoura', 'tesoura': 'pedra'}

    mao_jogador1, mao_jogador2 = mao_jogador1.lower(), mao_jogador2.lower()

    if mao_jogador1 not in ["pedra", "papel", "tesoura",] or mao_jogador2 not in ["pedra", "papel", "tesoura"]:
        return "Argumento inválido"

    if mao_jogador1 == mao_jogador2:
        return "Empate"

    if dicionario[mao_jogador1] == mao_jogador2:
        return mao_jogador2.capitalize() + " ganhou"
    else:
        return mao_jogador1.capitalize() + " ganhou"

@when('Capt. A joga "{}" e Capt. B joga "{}"')
def jogada_igual_pedra_com_pedra(context, mao_jogador1, mao_jogador2):
    context.resultado_jogada = jogo_jokenpo(mao_jogador1, mao_jogador2)

@then('acontece um "{}" e eles jogam novamente')
def igualdade(context, jogada):
    context.jogada = jogo_jokenpo("pedra", "pedra")
    assert context.jogada == "Empate"

@then('Capt. A ganhou com "{}"')
def tesoura_wins_captA(context, jogada):
    context.jogada = jogo_jokenpo("tesoura", "papel")
    assert context.jogada == 'Tesoura ganhou'

@then('Capt. B ganhou com "{}"')
def papel_wins_captB(context, jogada):
    context.jogada = jogo_jokenpo("PeDrA", "paPel")
    assert context.jogada == 'Papel ganhou'

@then('não é possivel jogar, retorna "{}"')
def jogo_errado(context, jogada):
    context.jogada = jogo_jokenpo("1", "Tesoura")
    assert context.jogada == "Argumento inválido"

Cenário: Capt. A joga "pedra" e Capt. B joga "pedra"
  Quando Capt. A joga "pedra" e Capt. B joga "pedra"
  Então acontece um "Empate" e eles jogam novamente

Cenário: Capt. A joga "tesoura" e Capt. B joga "papel
  Quando Capt. A joga "tesoura" e Capt. B joga "papel"
  Então Capt. A ganhou com "tesoura"

Cenário: Capt. A joga "PeDrA" e Capt. B joga "paPel
  Quando Capt. A joga "PeDrA" e Capt. B joga "paPel"
  Então Capt. B ganhou com "papel"

# language:pt
Funcionalidade: Jogo de Jokenpo
  A fim de passar o tempo ou decidir o primeiro a tirar time no futebol,
  eu como um jogador, escolho entre um elemento(pedra, papel ou tesoura) contra o outro jogador.
  Caso empate a jogada do jokenpo, os dois jogadores recomeçam a partida.
